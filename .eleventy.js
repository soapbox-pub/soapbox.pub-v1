module.exports = function(eleventyConfig) {
  eleventyConfig.addPassthroughCopy("src/fonts");
  eleventyConfig.addPassthroughCopy("src/images");
  eleventyConfig.addPassthroughCopy("src/styles");

  eleventyConfig.setTemplateFormats([
    "md",
    "njk",
    "css",
    "png",
    "gif",
    "jpg",
    "jpeg",
    "mp4",
    "ico",
    "svg",
    "webmanifest",
    "xml"
  ]);

  eleventyConfig.addCollection("blog", function(collectionApi) {
    return collectionApi.getFilteredByGlob("src/blog/**");
  });

  return {
    dir: { input: "src", output: "public" },
    markdownTemplateEngine: "njk",
  };
};
