---
layout: layouts/blog.njk
title: Soapbox FE v1.1 Released | Soapbox | Social Media Server
date: 2020-10-05
meta:
  description: Soapbox Frontend is a user interface for the Pleroma social media server. This release is a Quality of Life upgrade, bringing many new fixes and features.
  "og:title": Soapbox FE v1.1 Released
  "og:type": website
  "og:url": https://soapbox.pub/blog/soapbox-fe-v1.1-release/
  "og:image": https://soapbox.pub/blog/soapbox-fe-v1.1-release/bigben.jpeg
  "og:description": Soapbox Frontend is a user interface for the Pleroma social media server. This release is a Quality of Life upgrade, bringing many new fixes and features.
  "og:locale": en_US
blog:
  title: Soapbox FE v1.1 Released
  byline: By Alex Gleason
  screenshot: /blog/soapbox-fe-v1.1-release/soapbox-fe-1.1-screenshot.png
  thumb: /blog/soapbox-fe-v1.1-release/bigben.jpeg
  blurb: This release is so solid it makes v1.0 feel unfinished. Truly, Soapbox FE was put to the ultimate test — a real social network with 14,000 users. 185 issues were closed. Major new features were added. This update brings us up to speed with the latest Pleroma, released last month.
  lead: Soapbox Frontend is a user interface for the <a href="https://pleroma.social/" target="_blank">Pleroma</a> social media server. Today we're celebrating a major Quality of Life upgrade! 🎉
  code_url: https://gitlab.com/soapbox-pub/soapbox-fe
  date: Mon, Oct 5, 2020
---
<h2>What is Pleroma?</h2>
<p>Pleroma is a free and open source, federated, social media software allowing anyone to host their own online community. Servers can cross-communicate with other servers on the network thanks to federation. Soapbox&nbsp;FE is a sleek user interface that server admins can install on top of Pleroma.</p>

<h2>Quality of Life</h2>
<p>This release is so solid it makes v1.0 feel unfinished. Truly, Soapbox FE was put to the ultimate test &mdash; <em>a real social network with 14,000 users.</em></p>
<p>185 issues were closed. Major new features were added. This update brings us up to speed with <a href="https://pleroma.social/blog/2020/08/28/releasing-pleroma-2-1-0/" target="_blank">the latest Pleroma, released last month</a>.</p>

<h2>The Big Migration</h2>
<p>Spinster, the largest feminist server on the network, has now <a href="https://4w.pub/spinster-2-0-moving-from-mastodon-backend-to-pleroma/" target="_blank">been migrated</a> from Mastodon to Pleroma. This was a difficult challenge that pushed us to the limit.</p>
<p>Fortunately, our theory was correct, and Spinster now requires a fraction of the resources to operate. Before and after:</p>
<a href="spinster-mastodon-to-pleroma-comparison.png" target="_blank">
  <img class="gallery" src="spinster-mastodon-to-pleroma-comparison.png">
</a>
<p>Having a real userbase this time made all the difference. Users quickly identified bugs, and motivated us to make the software more robust to accomodate such a large community.</p>
<p>Spinster is not the only site migrated to Pleroma+Soapbox this round. Community member Anirudh Oppiliappan was the first outsider to successfully migrate Mastodon to Pleroma using our tool, and <a href="https://icyphox.sh/blog/mastodon-to-pleroma/" target="_blank">his blog post</a> hit the front page of Hacker News. Congrats Anirudh!</p>

<h2>Chats</h2>
<p>Without a doubt, Pleroma's biggest new feature is federated chats. Chats are private and 1-on-1. This exciting new way to communicate is only available on some backends, so you'll want your friends to be on Pleroma too.</p>
<img class="gallery" src="soapbox-chats.gif">
<p>"Mobile First" is the norm nowadays, but I think the Fediverse really needs a Facebook-style chat interface. So I built both. On desktop you can chat while browsing, and on mobile the chat will fill the screen.</p>
<img class="gallery" src="chat-panes.png">

<h2>Soapbox Config</h2>
<p>A big promise of Soapbox has always been server customization. As an admin, you're expected to bring your own brand. In older versions of Soapbox this required manually editing a file on the server, but now site settings such as logo and brand color can be customized right through the GUI.</p>
<img class="gallery" src="soapbox-config.gif">
<p><strong>Upgrade note:</strong> If you've already configured a soapbox.json file, simply load the config page once and hit "Save". This will move your soapbox.json into the database, and you can safely delete it from your server.</p>

<h2>Profile Hover Cards</h2>
<p>A classic Twitter feature, it took far too long for this to hit the Fediverse. Just hover your mouse over a user to see a preview of their account.</p>
<img class="gallery" src="profile-hover-card.gif">

<h2>Theme Toggle</h2>
<p>To protect your eyes, you can now quickly switch themes. In direct sunlight you'll likely need light mode to see the screen at all. While browsing at night, dark mode is easier on the eyes.</p>
<img class="gallery" src="theme-toggle.gif">

<h2>Markdown</h2>
<p>Posts are now Markdown-enabled by default! This will enable rich text formatting, and can be easily disabled on a per-post basis.</p>
<img class="gallery" src="markdown.png">

<h2>Instance Favicons</h2>
<p>In order to help newcomers understand the Fediverse, instance favicons can be shown next to each post. This feature needs to be enabled in Pleroma settings before it will take effect.</p>
<img class="gallery" src="favicons.png">
<p>This is the first step in an initiative to "gameify" the Fediverse, and make its unique qualities truly shine.</p>

<h2>Import Data</h2>
<p>Probably the most highly requested feature since the v1.0 release, users want to import their follows when moving from Mastodon to a Pleroma+Soapbox server. CSV exports from Mastodon can now be imported into Soapbox FE.</p>
<img class="gallery" src="import-data.png">

<h2>Bookmarks</h2>
<p>To easily revisit posts later, they can be bookmarked and revisited any time.</p>
<img class="gallery" src="bookmarks.png">

<h2>Audio Player</h2>
<p>Uploading sound clips displays an embedded audio player. Uploading multiple files will display the audio player in a modal window.</p>
<img class="gallery" src="audio-player.png">

<h2>Timeline Filters</h2>
<p>More advanced timeline filtering options now exist, such as filtering out DMs.</p>
<img class="gallery" src="timeline-filters.png">

<h2>Word Filters</h2>
<p>Certain words or phrases can be filtered out of your timeline.</p>
<img class="gallery" src="word-filters.png">

<h2>Multi-Factor Auth</h2>
<p>For improved security, users can enable multi-factor auth. After setup, this will prompt them to enter a time-based code from an OTP app on their phone whenever they log in.</p>
<img class="gallery" src="mfa.png">

<h2>Layout Improvements</h2>
<p>Space was optimized in this release so all three columns are used.</p>
<img class="gallery" src="layout.png">

<h2>Recurring Donations (Experimental)</h2>
<p>You may have noticed the "Donation" widget in these screenshots. That comes from <a href="https://gitlab.com/soapbox-pub/patron" target="_blank">Patron</a>, an experimental recurring donations platform. It's already running in production on a few servers, but isn't ready for broader adoption yet.</p>
<img class="gallery" src="patron.png">
<p>Our end goal for this is to allow servers to be self-funded by their users.</p>

<h2>Bug Fixes</h2>
<p>Some pretty annoying bugs were fixed. In particular there were some deep issues with scrollable lists and the post composer that got resolved.</p>
<ul>
  <li><strong>Post Composer</strong> &mdash; When the autosuggest box popped up, the cursor would jump to the end no matter what. This was fixed by preventing unnecessary re-renders of the autosuggest textarea.</li>
  <li><strong>Follower lists</strong> &mdash; While scrolling follower lists, entries would render invisibly and jump around the screen. This was due to duplicate entries and was resolved by storing the entries in a Set instead of a List.</li>
  <li><strong>Notifications</strong> &mdash; Same as above. Duplicate notifications were prevented by using an OrderedMap instead of a List.</li>
  <li><strong>Bundle size</strong> &mdash; Unnecessary libraries were removed or optimized, and thousands of lines of unused CSS were deleted. Because of this the bundle size stayed roughly the same despite all the new features.</li>
</ul>
<p>View the <a href="https://gitlab.com/soapbox-pub/soapbox-fe/-/blob/develop/CHANGELOG.md" target="_blank">CHANGELOG</a> for the complete list of bug fixes.</p>

<h2>Happy Halloween!</h2>
<p>One of the few missing features from Soapbox Legacy has returned in this release, just in time for Halloween. 🎃 Enable it in your Preferences.</p>
<video class="gallery" autoplay loop muted src="halloween.mp4"></video>

<h2>In the Future</h2>
<p>Just a small preview of a few ideas we're working on for a later release:</p>
<ul>
  <li><strong>Viewing remote timelines</strong> &mdash; clicking an instance favicon will show you a timeline of that server's posts. This is part of an initiative to "gameify" the Fediverse.</li>
  <li><strong>Page editor</strong> &mdash; we want to add basic CMS capabilities so you can manage a wiki on your server directly through Soapbox FE.</li>
  <li><strong>Record your voice</strong> &mdash; speak directly into the post composer to send an audio message.</li>
</ul>
<p>Have an idea? Submit it to our <a href="https://gitlab.com/soapbox-pub/soapbox-fe/-/issues" target="_blank">issue tracker</a>.</p>

<h2>Installing Soapbox FE</h2>
<p>Installation on an existing Pleroma server is a breeze, and can be done in less than 5 minutes. It is easy to remove if you change your mind.</p>
<p>To upgrade, repeat these steps.</p>
<h3>0. Install Pleroma</h3>
<p>If you haven't already, follow <a href="https://docs-develop.pleroma.social/backend/installation/debian_based_en/" target="_blank">this guide</a> to install Pleroma. If you're still running an old Pleroma version, be sure to upgrade it <em>before</em> upgrading Soapbox FE.</p>
<h3>1. Fetch the v1.1 build</h3>
<div class="code">curl -L https://gitlab.com/soapbox-pub/soapbox-fe/-/jobs/artifacts/v1.1.0/download?job=build-production -o soapbox-fe.zip</div>
<h3>2. Unpack</h3>
<div class="code">busybox unzip soapbox-fe.zip -o -d /opt/pleroma/instance</div>
<p><em>Or, if you installed the OTP release, unpack to this path instead:</em></p>
<div class="code">busybox unzip soapbox-fe.zip -o -d /var/lib/pleroma</div>
<p>
  <strong>That's it! &#127881; Soapbox FE is installed.</strong>
  The change will take effect immediately, just refresh your browser tab.
  It's not necessary to restart the Pleroma service.
</p>
<p>Note that it replaces Pleroma FE. (Running alongside Pleroma FE is <a href="https://docs.soapbox.pub/frontend/administration/install-subdomain/" target="_blank">also possible</a>). Logged-in users will have to re-authenticate.</p>
<p>For <strong>removal</strong>, run: <span class="code">rm /opt/pleroma/instance/static/index.html</span></p>
<h3>3. Customize it</h3>
<p>After logging in, click your avatar in the topbar, and navigate to Soapbox Config to customize it. Additional settings can be found under Admin Settings in the same menu.</p>
<img class="gallery" src="admin-link.png">

<h2>Fund the Soapbox Project</h2>
<p>Soapbox is funded entirely by donations. If you would like to support me, you can send me a donation. Your support is greatly appreciated, and every bit counts. Thank you!</p>
{% include "donate.njk" %}

<h2>Contributors</h2>
<p>A HUGE thank you to everyone who helped make this release a reality! In particular I'd like to thank M.K. Fain and the Spinster community for pushing to make this release perfect. And of course, the usual suspects. 😉</p>
<ul>
  <li>Curtis Rock</li>
  <li>Sean King</li>
  <li>Bárbara de Castro Fernandes</li>
  <li>M.K. Fain</li>
  <li>Lain, Feld, and the Pleroma dev team!</li>
</ul>
<p>To everyone else I did not mention, a huge thank you for being part of this journey! I'm eternally grateful, and excited about how this community is growing.</p>

<h2>Stay Updated</h2>
<p>To keep updated, follow me on the Fediverse at <a href="https://gleasonator.com/users/alex" target="_blank">@alex@gleasonator.com</a>. Thanks for your support!</p>
