---
layout: layouts/blog.njk
title: Soapbox FE v1.2 Released | Soapbox | Social Media Server
date: 2021-04-02
meta:
  description: Soapbox Frontend is a user interface for the Pleroma social media server. This release brings a slew of administrative improvements.
  "og:title": Soapbox FE v1.2 Released
  "og:type": website
  "og:url": https://soapbox.pub/blog/soapbox-fe-v1.2-release/
  "og:image": https://soapbox.pub/blog/soapbox-fe-v1.2-release/mods.png
  "og:description": Soapbox Frontend is a user interface for the Pleroma social media server. This release brings a slew of administrative improvements.
  "og:locale": en_US
blog:
  title: Soapbox FE v1.2 Released
  byline: By Alex Gleason
  blurb: With a slew of administration new features, this release builds on the stability of the v1.1 release. It brings us up to speed with Pleroma 2.3, released last month. Managing a Fediverse server can be challenging. That's why we brought the most important admin features into a convenient dashboard.
  screenshot: /blog/soapbox-fe-v1.2-release/soapbox-fe-1.2-screenshot.png
  thumb: /blog/soapbox-fe-v1.2-release/mods.png
  lead: Soapbox Frontend is a user interface for the <a href="https://pleroma.social/" target="_blank">Pleroma</a> social media server. Today we're celebrating a major feature release! 🎉
  code_url: https://gitlab.com/soapbox-pub/soapbox-fe
  date: Fri, Apr 2, 2021
---
<h2>What is Pleroma?</h2>
<p>Pleroma is a free and open source, federated, social media software allowing anyone to host their own online community. Servers can cross-communicate with other servers on the network thanks to federation. Soapbox&nbsp;FE is a sleek user interface that server admins can install on top of Pleroma.</p>

<h2>Battle Tested</h2>
<p>With a slew of administration new features, this release builds on the stability of the <a href="/blog/soapbox-fe-v1.1-release/">v1.1 release</a>. It brings us up to speed with <a href="https://pleroma.social/announcements/2021/03/02/pleroma-major-release-2-3-0/" target="_blank">Pleroma 2.3, released last month</a>.</p>

<h2>Admin Dashboard</h2>
<img class="gallery" src="dashboard.png">
<p>Managing a Fediverse server can be challenging. That's why we brought the most important admin features into a convenient dashboard.</p>
<img class="gallery" src="reports.gif">
<p>Reports can now be handled directly within Soapbox! Admins will get notified of reports. Take actions on reports, or close many reports quickly.</p>
<img class="gallery" src="registration-mode.gif">
<p>A quick registration switcher lets you close signups at your convenience. This is a useful for fighting spam or brigades.</p>
<p>The "Approval Required" option is better than ever. When enabled, you'll get a notification for new signups, which you can approve through the dashboard. I highly recommend this option for most communities.</p>
<img class="gallery" src="mod-log.png">
<p>Need to figure out what happened? Review the mod log to see a history of events.</p>

<h2>Inline Moderation</h2>
<img class="gallery" src="deletion-modal.gif">
<p>Admins can take actions on users directly from their profile or in timelines. When deleting a local user, you must tick the box to prevent mistakes.</p>

<h2>Remote Timelines</h2>
<img class="gallery" src="remote-timeline.gif">
<p>It's easy to see what's going on in remote servers. Click the favicon by any post and you will see the timeline for that instance. This promotes discoverability of federation, and helps people learn more about different communities on the Fediverse.</p>
<p>Note: you must enable instance favicons in AdminFE for this to take effect.</p>

<h2>Multiple Account Support</h2>
<p>To promote a project, idea, or brand, it is common to create a separate account. Now it is easy to switch between them. Simply sign in to each account, and they will all appear in your menu. Only local accounts are supported.</p>
<img class="gallery" src="profile-switcher.gif">
<p>In supported browsers, you can middle-click an account to activate it in a new tab.</p>

<h2>Remote Follow Button</h2>
<img class="gallery" src="remote-follow.png">
<p>To increase compatibility with third-party Fediverse software, remote follow is now possible.</p>

<h2>Responsive Follows</h2>
<img class="gallery" src="responsive-follow.gif">
<p>The follow button updates immediately when it goes through. Previously, you had to refresh the page.</p>

<h2>Registration Feedback</h2>
<img class="gallery" src="registration-modal.png">
<p>To date, "I can't log in" is still the number one complaint of Fediverse users. It is usually user error. We've done everything we can to streamline this process to make it easier to understand what's going on.</p>

<h2>Import Mutes</h2>
<img class="gallery" src="import-mutes.png">
<p>Importing mutes by CSV is now possible.</p>

<h2>Bug Fixes</h2>
<ul>
  <li><strong>Heart react</strong> &mdash; The heart emoji reaction stopped working with Pleroma's 2.3 update, but is now fixed across versions.</li>
  <li><strong>Block & mutes pagination</strong> &mdash; Previously only the first 20 results were shown for blocks and mutes. Now it supports endless scrolling.</li>
  <li><strong>Markdown</strong> &mdash; Due to problems with Markdown, it's now disabled by default, but configurable in Preferences.</li>
  <li><strong>Search loading</strong> &mdash; Display a loading animation while a search is in progress.</li>
  <li><strong>"Bot" badge</strong> &mdash; A "bot" badge is now displayed with bot accounts.</li>
  <li><strong>Mobile content</strong> &mdash; Sidebar content is now visible on mobile from the Server Information menu item.</li>
</ul>
<p>View the <a href="https://gitlab.com/soapbox-pub/soapbox-fe/-/blob/develop/CHANGELOG.md" target="_blank">CHANGELOG</a> for the complete list of bug fixes.</p>

<h2>Verifications (Blue Check)</h2>
<img class="gallery" src="blue-check.gif">
<p>For no other reason than just for fun, admins can give users a blue checkmark. This feature does not federate, but larger servers might find it useful.</p>

<h2>In the Future</h2>
<p>Here are some things we hope to include in the next version:</p>
<ul>
  <li><strong>Nested threading</strong> &mdash; To make it easier to see who replied to what, we want to adopt Reddit-style threading.</li>
  <li><strong>Notification dropdown</strong> &mdash; We'd like to make notifications a dropdown menu on desktop so you can check them without browsing away.</li>
</ul>
<p>Have an idea? Submit it to our <a href="https://gitlab.com/soapbox-pub/soapbox-fe/-/issues" target="_blank">issue tracker</a>.</p>

<h2>Installing Soapbox FE</h2>
<p>Installation on an existing Pleroma server is a breeze, and can be done in less than 5 minutes. It is easy to remove if you change your mind.</p>
<p>To upgrade, repeat these steps.</p>
<h3>0. Install Pleroma</h3>
<p>If you haven't already, follow <a href="https://docs-develop.pleroma.social/backend/installation/debian_based_en/" target="_blank">this guide</a> to install Pleroma. If you're still running an old Pleroma version, you should upgrade it.</p>
<h3>1. Fetch the v1.2 build</h3>
<div class="code">curl -L https://gitlab.com/soapbox-pub/soapbox-fe/-/jobs/artifacts/v1.2.3/download?job=build-production -o soapbox-fe.zip</div>
<h3>2. Unpack</h3>
<div class="code">busybox unzip soapbox-fe.zip -o -d /opt/pleroma/instance</div>
<p><em>Or, if you installed the OTP release, unpack to this path instead:</em></p>
<div class="code">busybox unzip soapbox-fe.zip -o -d /var/lib/pleroma</div>
<p>
  <strong>That's it! &#127881; Soapbox FE is installed.</strong>
  The change will take effect immediately, just refresh your browser tab.
  It's not necessary to restart the Pleroma service.
</p>
<p>Note that it replaces Pleroma FE. (Running alongside Pleroma FE is <a href="https://docs.soapbox.pub/frontend/administration/install-subdomain/" target="_blank">also possible</a>).</p>
<p>For <strong>removal</strong>, run: <span class="code">rm /opt/pleroma/instance/static/index.html</span></p>

<h2>Fund the Soapbox Project</h2>
<p>Soapbox is funded entirely by donations. If you would like to support me, you can send me a donation. Your support is greatly appreciated, and every bit counts. Thank you!</p>
{% include "donate.njk" %}

<h2>Update: 1.2.1 (Apr 6, 2021)</h2>
<p>A new minor release fixes the following bugs:</p>
<ul>
  <li>Redirect the user Home after logging in.</li>
  <li>Fix the "View context" button on videos.</li>
</ul>

<h2>Update: 1.2.2 (Apr 13, 2021)</h2>
<p>A new minor release fixes the following bugs:</p>
<ul>
  <li>Infinite loop when a secondary account is stored incorrectly.</li>
  <li>Emoji reacts not sent through notifications.</li>
</ul>

<h2>Update: 1.2.3 (Apr 18, 2021)</h2>
<p>A new minor release makes the following changes:</p>
<ul>
  <li>Redirect the user after registration.</li>
  <li>Fix file uploads ending in .blob</li>
  <li>Invalid auth users are now deleted from the user's browser.</li>
  <li>Twemoji is now bundled with Soapbox FE instead of relying on Pleroma BE to provide them.</li>
</ul>

<h2>Contributors</h2>
<p>A HUGE thank you to everyone who helped make this release a reality!</p>
<ul>
  <li>Bane</li>
  <li>Curtis Rock</li>
  <li>Isabell Deinschnitzel</li>
  <li>João Rodrigues</li>
  <li>Marcin Mikołajczak</li>
  <li>matrix07012</li>
  <li>NaiJi</li>
  <li>NEETzsche</li>
  <li>Ruine Zederberg</li>
  <li>Sean King</li>
  <li>The Pleroma dev team!</li>
</ul>
<p>To everyone else I did not mention, a huge thank you for being part of this journey!</p>

<h2>Stay Updated</h2>
<p>To keep updated, follow me on the Fediverse at <a href="https://gleasonator.com/users/alex" target="_blank">@alex@gleasonator.com</a>. Thanks for your support!</p>
