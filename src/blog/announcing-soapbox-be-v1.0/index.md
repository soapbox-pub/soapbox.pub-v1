---
layout: layouts/blog.njk
title: Announcing Soapbox BE v1.0 | Soapbox | Social Media Server
date: 2021-05-11
meta:
  description: Releasing a full social media server based on Pleroma.
  "og:title": Announcing Soapbox Backend
  "og:type": website
  "og:url": https://soapbox.pub/blog/announcing-soapbox-backend/
  "og:image": https://soapbox.pub/blog/announcing-soapbox-be-v1.0/nyancat.jpeg
  "og:description": Releasing a full social media server based on Pleroma.
  "og:locale": en_US
blog:
  title: Announcing Soapbox BE v1.0
  byline: By Alex Gleason
  blurb: Soapbox BE is a production ready Pleroma branch. It's being maintained alongside Pleroma, with additional bugfixes and features. Our goal is to move faster, while taking deliberate care to ensure clean code merges between projects. Soapbox BE contains code that has not yet been merged (or may never be merged) by Pleroma.
  screenshot: /blog/announcing-soapbox-be-v1.0/screenshot.png
  thumb: /blog/announcing-soapbox-be-v1.0/nyancat.jpeg
  lead: Soapbox is a social media server empowering communities online. Today we're releasing Soapbox&nbsp;BE&nbsp;v1.0 based on <a href="https://pleroma.social/" target="_blank">Pleroma</a>! 🎉
  code_url: https://gitlab.com/soapbox-pub/soapbox
  date: Tue, May 11, 2021
---
## Based on Pleroma

Soapbox BE is a production ready Pleroma branch based on Pleroma 2.3 stable.
It's being maintained alongside Pleroma, with additional bugfixes and features.
Our goal is to move faster, while taking deliberate care to ensure clean code merges between projects.

Soapbox BE contains code that has not yet been merged (or may never be merged) by Pleroma.

## A new foundation

A big part of this release was just laying the groundwork to support another Fediverse backend: an updated website, an <a href="https://gitlab.com/soapbox-pub/soapbox/-/issues" target="_blank">issue tracker</a>, and [proper documentation](/install/).

We are now free to do things that weren't possible before.

A full list of differences between Soapbox and Pleroma is <a href="https://gitlab.com/soapbox-pub/soapbox/-/blob/develop/CHANGELOG_soapbox.md" target="_blank">documented here</a>.

## Soapbox FE by default

<img class="gallery" src="/blog/soapbox-fe-v1.1-release/soapbox-fe-1.1-screenshot.png" alt="Soapbox FE">

Soapbox FE is the default frontend of Soapbox BE.
We think this is a good choice for growing the Fediverse, and it gives us more control over how the FE and BE interact.

It will still be possible to switch to another frontend as usual.
See the FAQ below for details.

## Rich media embeds

<img class="gallery" alt="YouTube embed" src="youtube-embed.png">

Share a link to a popular video site, and users can watch right from their timeline!
The following sites are tested to work:

- YouTube
- Vimeo
- PeerTube instances
- Giphy
- Soundcloud (audio player)
- Kickstarter
- Flickr
- anything on <a href="https://oembed.com/providers.json" target="_blank">this list</a>, and more

Most OEmbed compatible links will work.
First we try known URL patterns, then fall back to scraping the page (discovery).
Otherwise, it generates a link preview as usual.

Rich media embeds are sanitized and can only contain an iframe or image.
Unsafe embeds fall back to link previews.

## Configurable block behavior

On Twitter, blocking someone prevents them from seeing your posts.
On the Fediverse, it's a mixed bag.
Different servers handle blocks differently.

We think it makes sense for the default behavior to be Twitter-like, as it's what users expect.
You can now configure it in Admin FE, under "ActivityPub > Blockers visible"

**Note:** This setting cannot force a remote server to change their behavior, it only changes the behavior of your server.

## Bug fixes

- Domain blocks: reposts from a blocked domain are now correctly blocked. (<a href="https://gitlab.com/soapbox-pub/soapbox/-/merge_requests/11" target="_blank">!11</a>)
- Fixed some (not all) Markdown issues, such as broken trailing slash in links. (<a href="https://gitlab.com/soapbox-pub/soapbox/-/merge_requests/10" target="_blank">!10</a>)
- Don't crash so hard when email settings are invalid. (<a href="https://gitlab.com/soapbox-pub/soapbox/-/merge_requests/12" target="_blank">!12</a>)
- Return OpenGraph metadata on Soapbox FE routes. (<a href="https://gitlab.com/soapbox-pub/soapbox/-/merge_requests/14" target="_blank">!14</a>)

## Installing Soapbox

To install a fresh server, see our [install guide](/install/).

## Switching from Pleroma

Switching over is easy.
As long as you're on Pleroma 2.3, you can simply checkout the repo:

```sh
sudo -Hu pleroma bash
cd /opt/pleroma

git remote add soapbox https://gitlab.com/soapbox-pub/soapbox.git
git fetch soapbox --tags
git checkout -b soapbox soapbox-v1.0.0

MIX_ENV=prod mix deps.get
MIX_ENV=prod mix compile

# As root
systemctl restart pleroma
```

**Note:** Only the source version of Pleroma is supported.
If you're using the OTP release, you may need to switch to the source version first.

## FAQ

- **Will Soapbox FE continue to support vanilla Pleroma?** &mdash; Yes. Soapbox FE already uses version detection for compatibility across versions. It will continue to support Pleroma.

- **Will Soapbox BE stay updated with Pleroma?** &mdash; Yes. New stable releases of Pleroma will be merged into Soapbox BE. Soapbox BE releases will happen after Pleroma releases. Think of the relationship between Ubuntu and Debian.

- **Can I switch back to Pleroma?** &mdash; Yep, you can safely switch between Soapbox BE 1.0 and Pleroma 2.3. This release does not modify the database at all. Future releases will modify the database in reversible, non-destructive ways.

- **Can I use Pleroma FE with Soapbox BE?** &mdash; Technically, yes. You can remove Soapbox FE as the "primary" frontend within AdminFE, and it will fall back to Pleroma FE. This use case is unsupported by Pleroma FE devs, but it seems to work fine.

- **Can I use a customized frontend/custom build?** &mdash; Yep. Anything you put in `/opt/pleroma/instance` will override the default frontend. If you put a custom build here, remember to upgrade the FE and BE separately.

## Fund the Soapbox Project

Soapbox is funded entirely by donations. If you would like to support me, you can send me a donation. Your support is greatly appreciated, and every bit counts.

{% include "donate.njk" %}

## Thank you!

A huge thank you to everyone who has been so supportive of my efforts, from the donations to the moral support to development help.
Thank you to the Pleroma dev team for building a great foundation.
I really appreciate you all being a part of this and look forward to advancing the Fediverse together.
