---
layout: layouts/blog.njk
title: "Soapbox FE v1.3: The Cryptocurrency Release | Soapbox | Social Media Server"
date: 2021-07-02
meta:
  description: Bringing cryptocurrency to the Fediverse.
  "og:title": "Soapbox FE v1.3: The Cryptocurrency Release"
  "og:type": website
  "og:url": https://soapbox.pub/blog/soapbox-fe-v1.3-cryptocurrency-release/
  "og:image": https://soapbox.pub/blog/soapbox-fe-v1.3-cryptocurrency-release/crypto-icons-fantic.jpg
  "og:description": Bringing cryptocurrency to the Fediverse.
  "og:locale": en_US
blog:
  title: "Soapbox FE v1.3: The Cryptocurrency Release"
  byline: By Alex Gleason
  blurb: This update to the user interface for Pleroma+Soapbox brings cryptocurrency features to the Fediverse.
  screenshot: /blog/soapbox-fe-v1.3-cryptocurrency-release/soapbox-fe-1.3-screenshot.png
  thumb: /blog/soapbox-fe-v1.3-cryptocurrency-release/crypto-icons-fantic.jpg
  lead: Soapbox FE is the user interface for the Soapbox social media server. Today we're celebrating a major feature release centered around cryptocurrency! 🎉
  code_url: https://gitlab.com/soapbox-pub/soapbox-fe
  date: Fri, Jul 2, 2021
---

## On cryptocurrency

I've always believed integrated donations would be a necessary part of the Fediverse. Admins do all the heavy lifting; it's a thankless job. Meanwhile users want to help secure their new online home, but feel powerless to do so.

I have been running an experimental payment platform based on Stripe alongside my Soapbox servers for a while. It is clunky, vulnerable to deplatforming, and does not fit with the Free Software ethos. I am still working on it, but in the meantime I offer you a more future-proof solution: cryptocurrency.

Just a year ago cryptocurrency was considered "magical internet money." These days my mom is calling me on the phone asking if I know anything about Bitcoin because her friend's son got rich off it. Better late than never.

As a developer, cryptocurrency is a dream come true. The integrations are simple and seamless, and I can sleep well at night knowing it is unstoppable. So I've decided to put effort into making cryptocurrency first-class in Soapbox.

Does it completely solve payment processing? No. But it's a step in the right direction. It can't hurt. And maybe it will inspire people to use it more.

> &ldquo;If you build it, they will come.&rdquo;

## Homepage donation widget

<img class="gallery" src="crypto-homepage-widget.png" alt="Crypto homepage donation widget">

First off, right on the homepage is a widget inviting users to donate crypto. This is configurable by the admin to display any cryptocurrency, and any number of them (or not at all).

Mobile users can access "Cryptocurrency donations" from the mobile sidebar.

## Admin donations

<img class="gallery" src="crypto-admin-donate.png" alt="Crypto admin donation page">

Expanding the widget will bring you to a page of all available wallets (configured by the admin). The icons come from a library called <a href="https://github.com/spothq/cryptocurrency-icons" target="_blank">cryptocurrency-icons</a>.

From the admin's perspective, they just need to enter the addresses into a form.

## QR Code

<img class="gallery" src="crypto-modal.png" alt="Crypto donation modal window">

Clicking the QR code icon next to a wallet opens a modal window, so you can easily scan it from your phone.

## Block explorer

<img class="gallery" src="crypto-block-explorer.png" alt="Crypto block explorer link">

Clicking the arrow icon opens the address in a block explorer, giving a glimpse of what's inside.

## User profiles

<img class="gallery" src="crypto-user-profile.png" alt="Crypto user profile">

Best yet, users can accept donations too! Just put an address in a profile field, and use `$BTC`, `$ETH`, or any other symbol as the label. Soapbox FE will render a crypto address component in its place.

<img class="gallery" src="profile-metadata.png" alt="Crypto profile fields">

People have been putting crypto addresses in their profile fields for a while now. It is the natural place for them. Users are also accustomed to ticker symbols like `$BTC` (Twitter treats them like hashtags). The great thing about this solution is that it still makes sense to users of other frontends, it is predictable, and it even *federates*.

<img class="gallery" src="profile-metadata-mastodon.png" alt="Crypto profile fields on Mastodon">

Users of unsupported software (**like Mastodon, above**) will just see the profile fields as plain text. However, Mastodon users who use ticker symbols in the profile fields will have them displayed correctly as addresses when viewed through Soapbox FE.

## About the design

I put a lot of thought into creating the best possible design for these wallets. In the end I got something compact and reusable with powerful features.

<img class="gallery" src="wordpress-crypto-donate-box.png" alt="Crypto Donate Box plugin for WordPress">

I was first inspired by the design of a WordPress plugin called Crypto Donate Box (**shown above**). It displays a QR code, a copy button, and lets you click between tabs. However, this tabular design doesn't scale to tens (or hundreds) of currencies.

<img class="gallery" src="cryptoicons-co.png" alt="Cryptoicons.co">

Next, I noticed the grid of currencies from cryptoicons.co, and decided to try combining them. This is what I learned:

1. It's best to use an HTML `<input>` field for the address. This reduces errors copying it, it leaves things up to the browser, and it also makes it easy to implement a “copy” button.

2. It's best to avoid showing multiple QR codes on the screen at the same time. Doing so could cause the user’s phone to accidentally capture the wrong QR code, sending funds to the wrong address. This is why I ended up using a modal to display the QR code (and it made the design more compact as a result).

## Accepting cryptocurrency

<img class="gallery" src="trezor.jpg" alt="Trezor hardware wallet">

If you want to easily accept all kinds of cryptocurrencies, I recommend getting your hands on a <a href="https://trezor.io/" target="_blank">Trezor</a>. This will generate addresses for you to plug into your site.

Both models are fine, but the Model T is worth the price. I like Trezor because it's 100% open source *and* open hardware, down to the schematics.

## But wait, there's more!

This is my first time creating a release around a theme, but the truth is there's a lot more in this release than just cryptocurrency features.

## Scheduled posts

<img class="gallery" src="scheduled-post.png" alt="Scheduled post">

Users can now schedule posts! Combined with the ability to manage multiple accounts, this is a publisher's dream. It is easier than ever to manage accounts dedicated to a project or brand.

Scheduling a post places it into a queue where you can review and cancel them if needed. You may only schedule posts at least 5 minutes in the future.

Thank you to <a href="https://iddqd.social/@NEETzsche" target="_blank">NEETzsche</a> for first implementing this!

## User subscriptions

<img class="gallery" src="subscribe.png" alt="User subscriptions">

Yet another big improvement for brands, users can subscribe to get notifications whenever an account posts. Take this opportunity to subscribe to <a href="https://gleasonator.com/@soapbox" target="_blank">@soapbox@gleasonator.com</a> so you don't miss a project update.

Thank you to <a href="https://mstdn.io/@mkljczk" target="_blank">marcin mikołajczak</a> for first implementing this!

## Bug fixes & small improvements

There are A LOT more improvements in this release. These are a few highlights:

- Major performance boost
- Better support for multiple attachments
- Localization improvements

To see 20+ more changes, view the complete <a href="https://gitlab.com/soapbox-pub/soapbox-fe/-/blob/develop/CHANGELOG.md" target="_blank">CHANGELOG</a>.

## Installing Soapbox FE

Installation on an existing Soapbox or Pleroma server is a breeze, and can be done in less than 5 minutes. It is easy to remove if you change your mind.

To upgrade, repeat these steps.

### 0. Install Soapbox BE or Pleroma

If you haven't already, follow [this guide](/install) to install Soapbox. If you have a Pleroma installation, you may use that too.

### 1. Fetch the v1.3 build

```sh
curl -L https://gitlab.com/soapbox-pub/soapbox-fe/-/jobs/artifacts/v1.3.0/download?job=build-production -o soapbox-fe.zip
```

### 2. Unpack

```sh
busybox unzip soapbox-fe.zip -o -d /opt/pleroma/instance
```

*Or, if you installed the OTP release, unpack to this path instead:*

```sh
busybox unzip soapbox-fe.zip -o -d /var/lib/pleroma
```

**That's it! 🎉 Soapbox FE is installed.**
The change will take effect immediately, just refresh your browser tab.
It's not necessary to restart the Pleroma service.

Note that it replaces Pleroma FE. (Running alongside Pleroma FE is <a href="https://docs.soapbox.pub/frontend/administration/install-subdomain/" target="_blank">also possible</a>).

For *removal*, run: `rm /opt/pleroma/instance/static/index.html`

## Groups?

Groups are in the works! Follow along here:

- **Groups ActivityPub document:** <a href="https://gitlab.com/-/snippets/2138031" target="_blank">https://gitlab.com/-/snippets/2138031</a>
- **Client-Server API document:** <a href="https://gitlab.com/-/snippets/2137299" target="_blank">https://gitlab.com/-/snippets/2137299</a>
- **WIP backend code:** <a href="https://git.pleroma.social/alexgleason/pleroma/-/merge_requests/4" target="_blank">https://git.pleroma.social/alexgleason/pleroma/-/merge_requests/4</a>
- **WIP frontend code:** <a href="https://gitlab.com/soapbox-pub/soapbox-fe/-/merge_requests/554" target="_blank">https://gitlab.com/soapbox-pub/soapbox-fe/-/merge_requests/554</a>

## Fund the Soapbox Project

Soapbox is funded entirely by donations. If you would like to support me, you can send me a donation. Your support is greatly appreciated, and every bit counts.

{% include "donate.njk" %}

## Thank you!

As always, a HUGE thank you to the contributors of this release. The community has stepped up its game, and I look forward to continuing to build with you.
