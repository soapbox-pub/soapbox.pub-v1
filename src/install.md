---
layout: layouts/blog.njk
title: Installing Soapbox | Soapbox | Social Media Server
date: 2021-05-11
meta:
  description: How to install Soapbox, federated social media server compatible with Mastodon and PLeroma.
  "og:title": Installing Soapbox
  "og:type": website
  "og:url": https://soapbox.pub/install/
  "og:image": https://soapbox.pub/images/terminal.png
  "og:description": How to install Soapbox, federated social media server compatible with Mastodon and PLeroma.
  "og:locale": en_US
---
# Installing Soapbox

We recommend installing Soapbox on a **dedicated VPS (virtual private server) running Ubuntu 20.04 LTS**.
You should get your VPS up and running before starting this guide.

Some popular VPS hosting providers include:

- <a href="https://m.do.co/c/84e2ff1e790f" target="_blank">DigitalOcean</a> <sup>[referral link]</sup> &mdash; easy to use
- <a href="https://www.hetzner.com/cloud" target="_blank">Hetzner Cloud</a> &mdash; cheap
- <a href="https://buyvm.net/" target="_blank">BuyVM</a> &mdash; supports free speech

Expect to spend between **$10&ndash;15 USD/mo**, depending on the size of your community and how you choose to configure it.

You should already have a **domain name** from a registrar like <a href="https://www.namecheap.com/" target="_blank">Namecheap</a>.
Create an `A` record with your registrar pointing to the IP address of your VPS.

## 1. Shelling in

Once your VPS is running, you'll need to open a **terminal program** on your computer.
This will allow you to remotely connect to the server so you can run commands and install Soapbox.

![Terminal window](/images/terminal.png)

Linux and Mac users should have a terminal program pre-installed (it's just called **"Terminal"**), but Windows users may need to install <a href="https://www.cygwin.com/" target="_blank">Cygwin</a> first.

Once the terminal is open, connect to your server with the username and IP address provided by your VPS host.
It will likely prompt for a password.

```sh
ssh root@123.456.789
```

If you see a screen that looks like this, you've succeeded:

```
Welcome to Ubuntu 20.04.2 LTS (GNU/Linux 5.4.0-65-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Wed Apr 28 18:59:27 UTC 2021

  System load:  1.86                Processes:              201
  Usage of /:   66.1% of 146.15GB   Users logged in:        0
  Memory usage: 29%                 IPv4 address for ens18: 10.0.0.100
  Swap usage:   4%                  IPv4 address for ens19: 192.168.1.100

 * Pure upstream Kubernetes 1.21, smallest, simplest cluster ops!

     https://microk8s.io/

79 updates can be installed immediately.
0 of these updates are security updates.
To see these additional updates run: apt list --upgradable


Last login: Tue Apr 27 17:28:56 2021 from 98.198.61.119
root@gleasonator:~#
```

## 2. System setup

Before installing Soapbox, we have to prepare the system.

### 2.a. Install updates

Usually a fresh VPS already has outdated software, so run the following commands to update it:

```shell
apt update
apt upgrade
```

When prompted (`[Y/n]`) type `Y` and hit Enter.

### 2.b. Install system dependencies

Soapbox relies on some additional system software in order to function.
Install them with the following command:

```shell
apt install git build-essential postgresql postgresql-contrib cmake libmagic-dev imagemagick ffmpeg libimage-exiftool-perl nginx certbot unzip libssl-dev automake autoconf libncurses5-dev
```

### 2.c. Create the Pleroma user

For security reasons, it's best to run Soapbox as a separate user with limited access.

We'll create this user and call it `pleroma`:

```shell
useradd -r -s /bin/false -m -d /var/lib/pleroma -U pleroma
```

## 3. Install Soapbox

It's time to install Soapbox itself.
Let's get things up and running.

### 3.a. Downloading the source code

Download the Soapbox source code with git:

```shell
git clone -b {{ soapbox.be_ref }} https://gitlab.com/soapbox-pub/soapbox /opt/pleroma
chown -R pleroma:pleroma /opt/pleroma
```

Enter the source code directory, and become the pleroma user:

```shell
cd /opt/pleroma
sudo -Hu pleroma bash
```

(You should be the pleroma user in `/opt/pleroma` for the remainder of this section.)

### 3.b. Install Elixir

Soapbox uses the Elixir programming language (based on Erlang).
It's important we use a specific version of Erlang (23), so we'll use the <a href="https://asdf-vm.com/" target="_blank">asdf version manager</a> to install it.

Install asdf:

```shell
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.8.0
echo ". $HOME/.asdf/asdf.sh" >> ~/.bashrc
echo ". $HOME/.asdf/completions/asdf.bash" >> ~/.bashrc
exec bash
asdf plugin-add erlang
asdf plugin-add elixir
```

Finally, install Erlang/Elixir:

```shell
asdf install
```

(This will take about 15 minutes. ☕)

### 3.c. Compiling Soapbox

Install basic Elixir tools for compilation:

```shell
mix local.hex --force
mix local.rebar --force
```

Fetch Elixir dependencies:

```shell
mix deps.get
```

Finally, compile Soapbox:

```shell
MIX_ENV=prod mix compile
```

(This will take about 10 minutes. ☕)

### 3.d. Generate the configuration

It's time to preconfigure our instance.
The following command will set up some basics such as your domain name:

```sh
MIX_ENV=prod mix pleroma.instance gen
```

If you're happy with it, rename the generated file so it gets loaded at runtime:

```shell
mv config/generated_config.exs config/prod.secret.exs
```

### 3.e. Provision the database

The previous section also created a file called `config/setup_db.psql`, which you can use to create the database.

Exit back to the root user (for the remainder of this document):

```shell
exit
```

Execute the SQL file as the postgres user:

```shell
sudo -Hu postgres psql -f config/setup_db.psql
```

Now run the database migration as the pleroma user:

```shell
sudo -Hu pleroma bash -i -c 'MIX_ENV=prod mix ecto.migrate'
```

### 3.f. Start Soapbox

Copy the systemd service and start Soapbox:

```shell
cp /opt/pleroma/installation/pleroma.service /etc/systemd/system/pleroma.service
systemctl enable --now pleroma.service
```

If you've made it this far, congrats!
You're very close to being done.
Your Soapbox server is running, and you just need to make it accessible to the outside world.

## 4. Getting online

The last step is to make your server accessible to the outside world.
We'll achieve that by installing Nginx and enabling HTTPS support.

### 4.a. HTTPS

We'll use certbot to get an SSL certificate.

First, shut off Nginx:

```sh
systemctl stop nginx
```

Now you can get the certificate:

```shell
mkdir -p /var/lib/letsencrypt/
certbot certonly --email <your@emailaddress> -d <yourdomain> --standalone
```

Replace `<your@emailaddress>` and `<yourdomain>` with real values.

### 4.b. Nginx

Copy the example nginx configuration and activate it:

```shell
cp /opt/pleroma/installation/pleroma.nginx /etc/nginx/sites-available/pleroma.nginx
ln -s /etc/nginx/sites-available/pleroma.nginx /etc/nginx/sites-enabled/pleroma.nginx
```

You **must edit this file:**

```shell
nano /etc/nginx/sites-enabled/pleroma.nginx
```

Change all occurrences of `example.tld` with your site's domain name.
Use Ctrl+X, Y, Enter to save.

Finally, enable and start nginx:

```shell
systemctl enable --now nginx.service
```

🎉 Congrats, you're done!
Check your site in a browser and it should be online.

## 5. Post-installation

Below are some additional steps you can take after you've finished installation.

### Create your first user

If your instance is up and running, you can create your first user with administrative rights with the following task:

```shell
cd /opt/pleroma
sudo -Hu pleroma bash -i -c 'MIX_ENV=prod mix pleroma.user new <username> <your@emailaddress> --admin'
```

### Renewing SSL

If you need to renew the certificate in the future, uncomment the relevant location block in the nginx config and run:

```shell
certbot certonly --email <your@emailaddress> -d <yourdomain> --webroot -w /var/lib/letsencrypt/
```

## Questions

If you have questions or run into trouble, please [create an issue](https://gitlab.com/soapbox-pub/soapbox/-/issues) on the Soapbox GitLab.
